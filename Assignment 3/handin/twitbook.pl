% Return true if X exists in a list. Can give multiple results if the
% list contains the same item multiple times.
is_member(X, [X|_]).
is_member(X, [_|Tail]) :- is_member(X,Tail).

% Compare the length of two lists and be true if the second is 1 longer.
% This can be used to ensure that Different doesn't backtrack when it
% shouldn't.
cmpLen1([], [_]).
cmpLen1([_|T1],[_|T2]) :- cmpLen1(T1, T2).

% Take an element, a list and an empty list, and produce a new list that
% contains no instances of the element.
without(_, [], L, L).
without(X, [X|T], L1, L2) :- without(X, T, L1, L2).
without(X, [Y|T], L1, L2) :- without(X, T, [Y|L1], L2).

% Given a list and two elements, check if they are different by removing
% the first element from the list and then checking if the second
% element can be found in the new list. The list *must* contain both
% elements or it will always fail. It also cannot contain duplicates.
% The use of cmpLen1 is very inefficient.
different(L, X, Y) :- without(X, L, [], L1), cmpLen1(L1,L), is_member(Y, L1).

% Wrapper for different when used on a list of person(X,L) elements.
different_p(G, X, Y) :- different(G, person(X,_), person(Y,_)).

% Returns true if X isn't in a list.
notIn(_, _, []).
notIn(G, X, [Y|Tail]) :-
    different_p(G, X, Y),
    notIn(G, X, Tail).

% The first get operates on the Graph, the second on the list in a
% person node.
likes(G, X, Y) :- is_member(person(X,List),G), is_member(Y, List).

dislikes(G, X, Y) :- likes(G, Y, X), is_member(person(X,List),G),
                                     notIn(G, Y,List).

% given a list of nodes in G and node X, are all the nodes
% bidirectional to X?
bidirect_from(_, _, []).
bidirect_from(G, X, [H|T]) :- likes(G, H, X), bidirect_from(G, X, T).

popular(G, X) :- is_member(person(X,List),G), bidirect_from(G, X, List).

% analogous to bidirect_from but for direct edges from X.
direct_from(_, _, []).
direct_from(G, X, [H|T]) :- dislikes(G, H, X), direct_from(G, X, T).

outcast(G, X) :- is_member(person(X,List),G), direct_from(G, X, List).

% checks all nodes in G to see if they are bidirectional to X,
% and if not, that X does not have a direct edge instead.
bidirect_to(_, [], _).
bidirect_to(G, [person(H,_)|T], X) :- likes(G, H, X), likes(G, X, H),
                                      bidirect_to(G, T, X).
bidirect_to(G, [person(_,L)|T], X) :- notIn(G, X, L), bidirect_to(G, T, X).

friendly(G, X) :- is_member(person(X, _), G), bidirect_to(G, G, X).

% analogous to bidirect_to. Check all nodes if they have a direct
% edges to X, and if not, that it is not bidirectional.
direct_to(_, [], _).
direct_to(G, [person(H,_)|T], X) :- dislikes(G, X, H), direct_to(G, T, X).
direct_to(G, [person(_,L)|T], X) :- notIn(G, X, L), direct_to(G, T, X).

hostile(G, X) :- is_member(person(X, _), G), direct_to(G, G, X).

% OBSOLETE
% a_aux attempts to find a chain of likes on the condition that the next
% step in the chain has not been in it before.
%a_aux(G, X, Y, _) :- likes(G, X, Y).
%a_aux(G, X, Y, L) :- likes(G, X, Z), notIn(G, Z, L), a_aux(G, Z, Y, [Z|L]).

%admires(G, X, Y) :- different_p(G, X, Y), likes(G, X, Y).
%admires(G, X, Y) :- different_p(G, X, Y), likes(G, X, Z), a_aux(G, Z, Y, [Z]).

% OBSOLETE
% makes a list of all nodes Y in G for which X admires Y holds
%i_aux([], G, _, Y, L) :- notIn(G, Y, L).
%i_aux([person(K,_)|T], G, X, Y, L) :- admires(G, X, K), 
%                                      i_aux(T, G, X, Y, [K|L]).
%i_aux([K|T], G, X, Y, L) :- i_aux(T, G, X, Y, L).

%indifferent(G, X, Y) :- is_member(person(X, _), G), i_aux(G, G, X, Y, []).

% combine two lists of nodes with no repeats
union(_, [], L, L).
union(G, [H|T], L1, L2) :- notIn(G, H, L1), union(G, T, [H|L1], L2).
union(G, [H|T], L1, L2) :- is_member(H, L1), union(G, T, L1, L2).

% given a list of nodes, get their edges and combine all of them
cmb_edges(_, [], L, L).
cmb_edges(G, [H|T], L1, L2) :- is_member(person(H, Lin), G), 
                           union(G, Lin, L1, Lout), cmb_edges(G, T, Lout, L2).

% check if two lists are equally long
eq_len([], []).
eq_len([_|T1], [_|T2]) :- eq_len(T1, T2).

% check if some element from a list is *not* in the other list.
some_notIn(G, [H|_], Y) :- notIn(G, H, Y).
some_notIn(G, [_|T], Y) :- some_notIn(G, T, Y).

% check if the union of edges of a list of nodes is equal to itself
% if not, check that there is at least one new element, and then iterate
is_closure(G, X, YS, L) :- cmb_edges(G, X, YS, L), eq_len(L, YS).
is_closure(G, X, YS, L) :- cmb_edges(G, X, YS, L1), some_notIn(G, L1, YS),
                           is_closure(G, L1, L1, L).

% is YS the closure of X in G?                          
closure(G, X, YS) :- is_closure(G, [X], [X], YS).

% given the closure and that X and Y are different,
% X admires Y if Y is in the closure.
admires(G, X, Y) :- different_p(G, X, Y), closure(G, X, YS), is_member(Y, YS).

% conversely given the closure, X is indifferent to Y,
% if Y is not in the closure
indifferent(G, X, Y) :- closure(G, X, YS), notIn(G, Y, YS).










