:- begin_tests(twitbook).
:- use_module(library(lists)).

% get succeeds if X is in the list, and fails otherwise.
test(is_member) :-
    is_member(a, [b, c, a]).

test(is_memberFail, [fail]) :-
    is_member(a, [b, b, b]).

% without removes all X and also reverses the list order.
test(without, [setup(without(a, [b, c, a], [], L))]) :-
    L = [c, b].

% compare two elements belonging to the same list and see if they are
% different.
test(different) :-
    different([a,b], a, b).

test(differentFail, [fail]) :-
    different([a, b], a, a).

test(differentWrapper) :-
    different_p([person(a,[]), person(b,[])], a, b).

% Likes, or "is there an edge from a to b".
test(likes) :-
    likes([person(a, [b, c]), person(b, [a, c]), person(c, [])], a, b).

test(likesFail, [fail]) :-
    likes([person(a, [b, c]), person(b, [a, c]), person(c, [])], c, a).

test(likesEnum, [setup(likes([person(a, [b, c]), person(b, [a, c]),
                              person(c, [])], a, B))]) :-
    B = b.

% Dislikes, or "is there an edge from b to a, but no edge from a to b".
test(dislikes) :-
    dislikes([person(a, [b, c]), person(b, [a, c]), person(c, [])], c, a).

test(dislikesFail, [fail]) :-
    dislikes([person(a, [b, c]), person(b, [a, c]), person(c, [])], a, b).

test(dislikesEnum, [setup(dislikes([person(a, [b, c]), person(b, [a, c]),
                                    person(c, [])], c, A))]) :-
    A = a.

% Popular, or "all edges from a have a corresponding edge back".
test(popular) :-
    popular([person(a, [b, c]), person(b, [a, c]), person(c, [a])], a).

test(popularFail, [fail]) :-
    popular([person(a, [b, c]), person(b, [c]), person(c, [a])], a).

test(popularEnum, [setup(popular([person(a, [b, c]), person(b, [a, c]),
                                  person(c, [a])], A))]) :-
    A = a.

% Outcast, or "all edges from a have no edges back".
test(outcast) :-
    outcast([person(a, [b, c]), person(b, [c]), person(c, [b])], a).

test(outcastFail, [fail]) :-
    outcast([person(a, [b, c]), person(b, [c]), person(c, [b, a])], a).

test(outcastEnum, [setup(outcast([person(a, [b, c]), person(b, [c]),
                                  person(c, [b])], A))]) :-
    A = a.

% Friendly, or "all edges *to* a have an edge back".
test(friendly) :-
    friendly([person(a, [b, c]), person(b, [a]), person(c, [b])], a).

test(friendlyFail, [fail]) :-
    friendly([person(a, [c]), person(b, [a]), person(c, [a, b])], a).

test(friendlyEnum, [setup(friendly([person(a, [b, c]), person(b, [a]),
                                    person(c, [b])], A))]) :-
    A = a.

test(friendlyIsMember, [fail]) :-
    friendly([], a).

% Hostile, or "no edges *to* a have an edge back".
test(hostile) :-
    hostile([person(a, []), person(b, [a, c]), person(c, [a, b])], a).

test(hostileFail, [fail]) :-
    hostile([person(a, [b]), person(b, [a, c]), person(c, [a, b])], a).

test(hostileEnum, [setup(hostile([person(a, []), person(b, [a, c]),
                                  person(c, [a, b])], A))]) :-
    A = a.

test(hostileIsMember, [fail]) :-
    hostile([], a).

% Admires, or "there is a chain of edges from a to b".
test(admires) :-
    admires([person(a, [b, d]), person(b, [c]), person(c, [a, e]),
             person(d, [a]), person(e, [c])], a, e).

test(admiresFail, [fail]) :-
    admires([person(a, [b, d]), person(b, [c]), person(c, [a]),
             person(d, [a]), person(e, [c])], a, e).

test(admiresCycle) :-
    admires([person(a, [b, d]), person(b, [c]), person(c, [a, e]),
             person(d, [a]), person(e, [c])], e, d).

test(admiresCycleFail, [fail]) :-
    admires([person(a, [b]), person(b, [c]), person(c, [a, e]),
             person(d, [a]), person(e, [c])], e, d).

test(admiresCycleInterconnectedFail, [fail]) :-
    admires([person(a, [b, c, e]), person(b, [c, a, e]), person(c, [a, e, b]),
             person(d, [a]), person(e, [c, b, a])], e, d).

% Union operation testing - combine two lists with no repeats
test(union1, [setup(union([person(a, []), person(b, []), person(c, [])],
                    [a, b], [c], L))]) :- L = [b, a, c].
test(union2, [setup(union([person(a, []), person(b, []), person(c, [])],
                    [a, b], [b, c], L))]) :- L = [a, b, c].
test(union3, [setup(union([person(a, []), person(b, []), person(c, [])],
                    [a], [c], L))]) :- L = [a, c].
    
% cmb_edges (combine edges) testing
% generate the union of the edges of a list of nodes
test(cmb1, [setup(cmb_edges([person(a, [b]), person(b, [])],
                            [a], [a], L))]) :- L = [b, a].
test(cmb2, [setup(cmb_edges([person(a, [b, d]), person(b, [c]), person(c, [a]),
                             person(d, [a]), person(e, [c])],
                             [a], [a], L))]) :- L = [d, b, a].
test(cmb3, [setup(cmb_edges([person(a, [b, d]), person(b, [c]), person(c, [a]),
                             person(d, [a]), person(e, [c])],
                             [d, b, a], [d, b, a], L))]) :- L = [c, d, b, a].
  
% equal length, are two lists equally long
test(eqTrue) :- eq_len([1, 2, 3], [3, 2, 1]).
test(eqFalse, [fail]) :- eq_len([1, 2], [3, 2, 1]).
  
% closure, or "get all reachable nodes from x"
test(closure, [setup(closure([person(a, [b, d]), person(b, [c]), person(c, [a]),
                             person(d, [a]), person(e, [c])],
                             a, L))]) :- L = [c, d, b, a].
  
% Indifferent, or "there is no chain of edges from a to b".
test(indifferent) :-
    indifferent([person(a, [b, d]), person(b, [c]), person(c, [a]),
                 person(d, [a]), person(e, [c])], a, e).
             
test(indifferentFail, [fail]) :-
    indifferent([person(a, [b, d]), person(b, [c]), person(c, [a, e]),
                 person(d, [a]), person(e, [c])], a, e).
            
test(indifferentCycle) :-
    indifferent([person(a, [b]), person(b, [c]), person(c, [a, e]),
                 person(d, [a]), person(e, [c])], e, d).
             
test(indifferentCycleFail, [fail]) :-
    indifferent([person(a, [b, d]), person(b, [c]), person(c, [a, e]),
                 person(d, [a]), person(e, [c])], e, d).
            
:- end_tests(twitbook).
