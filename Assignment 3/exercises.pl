reverse([],[]).
reverse([H|T], RevList):-
    reverse(T, RevT), append(RevT, [H], RevList).

palindrome(List) :- reverse(List,Rev), Rev = List.

