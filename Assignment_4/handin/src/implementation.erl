-module(implementation).

-export([new/1, request/4, route/4, drop_group/2, insert_paths/3, 
         server_p/2, get_func/2, get_longest/2, filter_prefix/2]).

new(_Global) ->
    S_id = spawn(implementation, server_p, [_Global, maps:new()]),
    {ok, S_id}.

%% The underlying server process. It handles maintaining state
%% and the route table. It also is responsible for error handling.
%% The server process is however also a bottleneck for actions, since
%% all action execution is done as part of this thread. Only non-global
%% state is kept in other processes, which removes all the benefits of
%% concurrency. An improvement would involve a wrapper process for each
%% action, and a change to the values in the key-value store.
server_p(GState, Routes) ->
    receive
        %% This handles "add route" messages
        {route, Paths, Func, Init, From} -> 
            Id = spawn(fun() -> context(Init) end),
            Routes_ = insert_paths(Paths, Routes, {Func, Id}),
            Tag = make_ref(),
            From ! {ok, Tag},
            server_p(GState, Routes_);
            
        %% This handles requests
        {request, {Path, Args}, From, Ref} ->
            try get_func(Path, Routes) of
                {Func, Id} -> Id ! {self(), getContext},
                              %% retrieves the local state
                              LState = receive
                                  {State} -> State
                              after
                                  5000 -> From ! {Ref, {500, timeout}},
                                  server_p(GState, Routes)
                              end,
                              %% runs the function in the server thread
                              try Func({Path, Args}, GState, LState) of
                                  {no_change, Content} ->
                                      From ! {Ref, {200, Content}},
                                      server_p(GState, Routes);
                                  {new_state, Content, NewState} ->
                                      Id ! {NewState, setContext},
                                      From ! {Ref, {200, Content}},
                                      server_p(GState, Routes)
                              %% catches any error and returns the
                              %% error type along with code 500
                              %% this should be replaced with
                              %% something that only catches specific
                              %% errors, not to mention it should be
                              %% kept out of the server thread, since
                              %% a bad error shouldn't crash the whole
                              %% server, only the specific route.
                              catch
                                  _:Error -> From ! {Ref, {500, Error}},
                                             server_p(GState, Routes)
                              end
            %% catches errors in retrieving the function from the
            %% key-value table.
            catch
                _:no_such_path -> From ! {Ref, {404, not_found}},
                                          server_p(GState, Routes)
            end;
        %% this is used for cleaning up during testing.
        {terminate} -> exit("Finished")
    end.

%% Process to keep track of state for a routing group.
%% it should be replaced by a wrapper function that holds
%% state and also holds the function and handles the updates
%% so that the implementation can take advantage of concurrency.
context(State) ->
    receive
        {Id, getContext} -> Id ! {State}, context(State);
        {NewState, setContext} -> context(NewState)
    end.
    
%% Handles insertion of prefixes for a given function/process.
insert_paths([], Map, _) -> Map;
insert_paths([Prefix|Tail], Map, Id) -> 
    Map_ = case maps:is_key(Prefix, Map) of
        true -> maps:update(Prefix, Id, Map);
        false -> maps:put(Prefix, Id, Map)
    end,
    insert_paths(Tail, Map_, Id).

%% Get the value associate with the key that is the
%% longest prefix of the first argument. In this case
%% the value is supposed to be a PID.
get_func(Path, Map) ->
    Keys = maps:keys(Map),
    Keys_ = filter_prefix(Path, Keys),
    Prefix = get_longest("", Keys_),
    try
        maps:get(Prefix, Map)
    catch
        error:{badkey,_} -> throw(no_such_path)
    end.

%% Finds all strings in a list that are a prefix of the first argument
%% and returns them in a list.
filter_prefix(_, []) -> [];
filter_prefix(Path, [H|T]) ->
    case lists:prefix(H, Path) of
        true -> [H] ++ filter_prefix(Path, T);
        false -> filter_prefix(Path, T)
    end.

%% Iterates through a list of strings, keeping track of the longest one
%% encountered so far. At the end, the longest is returned.
get_longest(Curr, []) -> Curr;
get_longest(Curr, [H|T]) ->
    case length(Curr) < length(H) of
        true -> get_longest(H, T);
        false -> get_longest(Curr, T)
    end.

request(_Flamingo, _Request, _From, _Ref) ->
    _Flamingo ! {request, _Request, _From, _Ref}.
    
route(_Flamingo, _Path, _Fun, _Arg) ->
    _Flamingo ! {route, _Path, _Fun, _Arg, self()},
    receive
        {ok, Id} -> {ok, Id};
        {error, Reason} -> {error, Reason}
    end.
    
    
    
drop_group(_Flamingo, _Id) ->
    not_implemented.