-module(counter).

-export([server/0]).

server() ->
    {ok, F} = flamingo:new("Mood"),
    flamingo:route(F, ["/inc_with", "/dec_with"], fun count/3, 0),
    F.

count({"/inc_with", Args}, _, Count) ->
    New_count = case Args of
        [{"x", Val} | _] ->
            case list_to_integer(Val) of
                X when X > 0 -> Count + X;
                _ -> Count + 1
            end;
        _ -> Count + 1
    end,
    {new_state, integer_to_list(New_count), New_count};
count({"/dec_with", Args}, _, Count) ->
    New_count = case Args of
                [{"x", Val} | _] ->
            case list_to_integer(Val) of
                X when X > 0 -> Count - X;
                _ -> Count - 1
            end;
        _ -> Count - 1
    end,
    {new_state, integer_to_list(New_count), New_count}.