-module(mood).

-export([server/0]).

server() ->
    {ok, F} = flamingo:new("Mood"),
    flamingo:route(F, ["/moo", "/mood"], fun mood_change/3, sad),
    F.

mood_change({"/moo", _}, _, Mood) ->
    case Mood of
        sad -> {new_state, "That's funny", happy};
        happy -> {no_change, "That's funny"}
    end;
mood_change({"/mood", _}, _, Mood) ->
    case Mood of
        sad -> {no_change, "Sad"};
        happy -> {no_change, "Happy!"}
    end.