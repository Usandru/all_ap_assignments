-module(flamingo).

-export([new/1, request/4, route/4, drop_group/2]).

new(_Global) ->
    implementation:new(_Global).

request(_Flamingo, _Request, _From, _Ref) ->
    implementation:request(_Flamingo, _Request, _From, _Ref).

route(_Flamingo, _Path, _Fun, _Arg) ->
    implementation:route(_Flamingo, _Path, _Fun, _Arg).

drop_group(_Flamingo, _Id) ->
    not_implemented.
