-module(test_implementation_tests).
-include_lib("eunit/include/eunit.hrl").
-import(implementation,[insert_paths/3, get_longest/2, filter_prefix/2, get_func/2, route/4, request/4]).

%% test setup for insert_paths/3
insert_path_test_() ->
    {setup,
     fun build_maps/0,
     fun end_map/1,
     fun insert_path_testing/1}.

build_maps() -> 
    M1 = maps:new(),
    M2 = maps:put("test", 1, M1),
    {M1, M2}.

end_map(_) -> ok.
     
%% tests the insert_paths/3 aux function on various states of the
%% route map.
insert_path_testing({M1,M2}) ->
    [?_assertEqual(insert_paths([], M1, dummy), #{}),
     ?_assertEqual(insert_paths(["path"], M1, dummy), #{"path" => dummy}),
     ?_assertEqual(insert_paths(["test"], M2, 2), #{"test" => 2}),
     ?_assertEqual(insert_paths(["path"], M2, 1), #{"test" => 1, "path" => 1}),
     ?_assertEqual(insert_paths(["p1", "p2"], M2, 1),
                   #{"test" => 1, "p1" => 1, "p2" => 1})
    ].

%% test setup for the three aux functions for getting prefixes
%% end_map/1 is reused since clean-up isn't needed.
get_prefix_test_() ->
    {setup,
     fun build_string_map/0,
     fun end_map/1,
     fun get_prefix_testing/1}.
     
build_string_map() ->
    Map1 = maps:new(),
    Map2 = maps:put("t1", 1, Map1),
    Map3 = maps:put("test", 2, Map2),
    Map4 = maps:put("te", 3, Map3),
    {Map1,Map2,Map3,Map4}.

%% tests the three aux functions for getting values from the
%% route map. It is assumed that the map is a map and not some
%% other data type.    
get_prefix_testing({M1,M2,_,M4}) ->
    [?_assertEqual(get_longest("", maps:keys(M1)), ""),
     ?_assertEqual(get_longest("", maps:keys(M2)), "t1"),
     ?_assertEqual(get_longest("", maps:keys(M4)), "test"),
     ?_assertEqual(filter_prefix("test", maps:keys(M1)), []),
     ?_assertEqual(filter_prefix("test", maps:keys(M2)), []),
     ?_assertEqual(filter_prefix("test", maps:keys(M4)), ["te", "test"]),
     ?_assertThrow(no_such_path, get_func("test", M1)),
     ?_assertEqual(get_func("t1", M2), 1),
     ?_assertEqual(get_func("test", M4), 2)
    ].
    
%% Tests of the full implementation on the various clients.
server_test_() ->
    {setup,
     fun build_servers/0,
     fun end_servers/1,
     fun run_servers/1}.
     
build_servers() ->
    Hello = hello:server(),
    Greetings = greetings:server(),
    Mood = mood:server(),
    Counter = counter:server(),
    {Hello, Greetings, Mood, Counter}.
    
end_servers({H, G, M, C}) ->
    H ! {terminate},
    G ! {terminate},
    M ! {terminate},
    C ! {terminate}.

%% Helper function to handle passing requests and receiving
%% replies.
server_tester(ServerId, Path, Args) ->
    Me = self(),
    Ref = make_ref(),
    flamingo:request(ServerId, {Path, Args}, Me, Ref),
    receive
        {Ref, Reply} -> Reply
    end.

%% Runs tests of all the four server implementations to check how
%% they handle and in particular if they change state correctly.
run_servers({H, G, M, C}) ->
    %% The first server tested is the provided "Greetings" server.
    %% there is only one test, it takes two lines due to the length
    %% of the answer string.
    [?_assertEqual(server_tester(G, "/hello", [{"name", "Jane"}]),
    {200, "Greetings Jane\nYou have reached The Flamingo Server"}),
    
    %% The second server tested is the "Hello" server.
     ?_assertEqual(server_tester(H, "/hello", []), {200, "Hello my friend"}),
     ?_assertEqual(server_tester(H, "/goodbye", []),
                   {200, "Sad to see you go."}),
     
    %% The third server tested is the "Mood" server.
     ?_assertEqual(server_tester(M, "/mood", []), {200, "Sad"}),
     ?_assertEqual(server_tester(M, "/moo", []), {200, "That's funny"}),
     ?_assertEqual(server_tester(M, "/mood", []), {200, "Happy!"}),
     ?_assertEqual(server_tester(M, "/moo", []), {200, "That's funny"}),
     
    %% The fourth server tested is the "Counter" server.
     ?_assertEqual(server_tester(C, "/inc_with", []), {200, "1"}),
     ?_assertEqual(server_tester(C, "/inc_with", [{"y", "6"}]), {200, "2"}),
     ?_assertEqual(server_tester(C, "/inc_with", [{"x", "5"}]), {200, "7"}),
     ?_assertEqual(server_tester(C, "/dec_with", [{"x", "5"}]), {200, "2"}),
     ?_assertEqual(server_tester(C, "/dec_with", [{"y", "9"}]), {200, "1"}),
     ?_assertEqual(server_tester(C, "/dec_with", []), {200, "0"}),
     
    %% Finally bad inputs are tested.
    ?_assertEqual(server_tester(M, "/boo", []), {404, not_found}),
    ?_assertEqual(server_tester(G, "/hello", [{"nah", "bad"}]),
                  {500, function_clause})
    ].