-module(hello).

-export([server/0]).

server() ->
    {ok, F} = flamingo:new("Hello"),
    flamingo:route(F, ["/hello"], fun hello/3, none),
    flamingo:route(F, ["/goodbye"], fun goodbye/3, none),
    F.

hello(_, _, _) ->
    {no_change, "Hello my friend"}.
    
goodbye(_, _, _) ->
    {no_change, "Sad to see you go."}.