-- This is a skeleton file for you to edit

{-# OPTIONS_GHC -W #-}  -- Just in case you forgot...

module Arithmetic
  (
  showExp,
  evalSimple,
  extendEnv,
  evalFull,
  evalErr,
  showCompact,
  evalEager,
  evalLazy
  )

where

import Definitions
import Data.Maybe


showExp :: Exp -> String
showExp (Cst n)
    | n < 0     = "(" ++ show n ++ ")"
    | otherwise = show n
showExp (Add xp1 xp2) = "(" ++ showExp xp1 ++ "+" ++ showExp xp2 ++ ")"
showExp (Mul xp1 xp2) = "(" ++ showExp xp1 ++ "*" ++ showExp xp2 ++ ")"
showExp (Sub xp1 xp2) = "(" ++ showExp xp1 ++ "-" ++ showExp xp2 ++ ")"
showExp (Div xp1 xp2) = "(" ++ showExp xp1 ++ "/" ++ showExp xp2 ++ ")"
showExp (Pow xp1 xp2) = "(" ++ showExp xp1 ++ "^" ++ showExp xp2 ++ ")"
showExp _ = error "Input contains expressions not defined in this function."

evalSimple :: Exp -> Integer
evalSimple (Cst n) = n
evalSimple (Add xp1 xp2) = evalSimple xp1 + evalSimple xp2
evalSimple (Sub xp1 xp2) = evalSimple xp1 - evalSimple xp2
evalSimple (Mul xp1 xp2) = evalSimple xp1 * evalSimple xp2
evalSimple (Div xp1 xp2) = evalSimple xp1 `div` evalSimple xp2
evalSimple (Pow xp1 xp2)
    | evalSimple xp2 < 0  = error "exponent value is below zero"
    | evalSimple xp2 == 0 = seq (evalSimple xp1) 1
    | otherwise           = evalSimple xp1 ^ evalSimple xp2
evalSimple _ = error "Input contains expressions not defined in this function."


-- extendEnv makes no assumptions about the Env it extends, 
-- it will work on arbitrary Env functions
extendEnv :: VName -> Integer -> Env -> Env
extendEnv v n r m = if m == v then Just n else r m

evalFull :: Exp -> Env -> Integer
evalFull (Cst n) _ = n
evalFull (Var v) r
    | isNothing (r v) = error ("No value assigned to var " ++ v)
    | otherwise        = fromJust (r v)
evalFull (Add xp1 xp2) r = evalFull xp1 r + evalFull xp2 r
evalFull (Sub xp1 xp2) r = evalFull xp1 r - evalFull xp2 r
evalFull (Mul xp1 xp2) r = evalFull xp1 r * evalFull xp2 r
evalFull (Div xp1 xp2) r = evalFull xp1 r `div` evalFull xp2 r
evalFull (Let v aux body) r = evalFull body 
                                       (extendEnv v (evalFull aux r) r)
evalFull (If t xp1 xp2) r
    | evalFull t r == 0   = evalFull xp2 r
    | otherwise           = evalFull xp1 r
evalFull (Pow xp1 xp2) r
    | evalFull xp2 r < 0  = error "power value is below zero"
    | evalFull xp2 r == 0 = seq (evalFull xp1 r) 1
    | otherwise           = evalFull xp1 r ^ evalFull xp2 r
evalFull (Sum v from to body) r
    | evalFull from r > evalFull to r = 0
    | otherwise                       = sum [evalFull (Let v (Cst x) body) r 
                                            | x <- [k..n]]
                                            where
                                               k = evalFull from r
                                               n = evalFull to r


-- This function handles passing outcomes from two subtrees,
-- transformed by some function if neither outcome is an error.
-- It is mainly used for the basic operations
bindErr :: Either ArithError Integer -> Either ArithError Integer
        -> ((Integer,Integer) -> Either ArithError Integer)
        -> Either ArithError Integer
bindErr (Left err) _ _ = Left err
bindErr _ (Left err) _ = Left err
bindErr (Right x) (Right y) k  = k (x, y)

-- Since using guards in lambdas is not possible using default GHC,
-- this function can be passed to bindErr
-- to handle the special behaviour in division.
divHelper :: (Integer,Integer) -> Either ArithError Integer
divHelper (x,y)
    | y == 0    = Left EDivZero
    | otherwise = Right (x `div` y)

-- Analoguous to divHelper, this function can be passed to bindErr
-- to handle the special behaviour needed for power.
powHelper :: (Integer,Integer) -> Either ArithError Integer
powHelper (x,y)
    | y < 0     = Left ENegPower
    | y == 0    = Right 1
    | otherwise = Right (x ^ y)
  
-- Since the if-function only evaluates one path,
--it cannot be encapsulated in bindErr.
-- For evaluating the branches, bindErr with a dummy value is passed.
-- The dummy value is ignored in the lambda.
bindIf :: Either ArithError Integer -> Either ArithError Integer
       -> Either ArithError Integer -> Either ArithError Integer
bindIf (Left err) _ _ = Left err
bindIf (Right n) x y
    | n == 0    = bindErr (Right 0) y (\(_,y) -> Right y)
    | otherwise = bindErr x (Right 0) (\(x,_) -> Right x)

-- To ensure that errors in let expressions can be properly returned,
-- the expression is evaluated immediately,
-- after which bindErr may continue computation.
bindLet :: Either ArithError Integer
        -> VName -> Exp -> Env
        -> Either ArithError Integer
bindLet (Left err) _ _ _ = Left err
bindLet (Right n) v xp r = bindErr (evalErr xp (extendEnv v n r)) 
                                   (Right 0) (\(x,_) -> Right x)

-- In the style of divHelper and powHelper, 
-- sumHelper handles special behaviour 
-- after bindErr has checked the values of "from" and "to"
-- in the sum expression.
sumHelper :: VName -> Exp -> Env -> (Integer,Integer)
          -> Either ArithError Integer
sumHelper v body r (x,y)
    | x > y     = Right 0
    | otherwise = sumWrapper [evalErr (Let v (Cst i) body) r 
                             | i <- [x..y]]

-- This function exists purely to extract the first element of the sum-list
-- and initialize sumBind with 0 in the accumulator value.
sumWrapper :: [Either ArithError Integer] -> Either ArithError Integer
sumWrapper [] = Left (EOther "Empty sum")
sumWrapper (x:xs) = sumBind x 0 xs

-- this function handles the error-passing and summation
-- of the list of values in the sum expression, 
-- terminating on detecting an error or if the list of values is empty.
sumBind :: Either ArithError Integer -> Integer 
        -> [Either ArithError Integer] -> Either ArithError Integer
sumBind (Left err) _ _ = Left err
sumBind (Right n) acc [] = Right (n + acc)
sumBind (Right n) acc (x:xs) = sumBind x (n + acc) xs


evalErr :: Exp -> Env -> Either ArithError Integer
evalErr (Cst n) _ = Right n
evalErr (Var v) r
    | isNothing (r v)  = Left (EBadVar v)
    | otherwise        = Right (fromJust (r v))
evalErr (Add xp1 xp2) r = bindErr (evalErr xp1 r) (evalErr xp2 r) 
                                  (\(x, y) -> Right (x + y))
evalErr (Sub xp1 xp2) r = bindErr (evalErr xp1 r) (evalErr xp2 r) 
                                  (\(x, y) -> Right (x - y))
evalErr (Mul xp1 xp2) r = bindErr (evalErr xp1 r) (evalErr xp2 r) 
                                  (\(x, y) -> Right (x * y))
evalErr (Div xp1 xp2) r = bindErr (evalErr xp1 r) (evalErr xp2 r)
                                  divHelper
evalErr (Pow xp1 xp2) r = bindErr (evalErr xp1 r) (evalErr xp2 r)
                                  powHelper
evalErr (If t xp1 xp2) r = bindIf (evalErr t r) 
                                  (evalErr xp1 r) (evalErr xp2 r)
evalErr (Let v aux body) r = bindLet (evalErr aux r) v body r
evalErr (Sum v from to body) r = bindErr (evalErr from r) (evalErr to r)
                                         (sumHelper v body r)



-- optional parts (if not attempted, leave them unmodified)

showCompact :: Exp -> String
showCompact = undefined

evalEager :: Exp -> Env -> Either ArithError Integer
evalEager = undefined

evalLazy :: Exp -> Env -> Either ArithError Integer
evalLazy = undefined