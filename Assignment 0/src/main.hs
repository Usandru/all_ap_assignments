import Arithmetic
import Definitions

zero = Cst 0
one = Cst 1
two = Cst 2
three = Cst 3
four = Cst 4
five = Cst 5

negone = Cst (-1)
negtwo = Cst (-2)
negthree = Cst (-3)
negfour = Cst (-4)
negfive = Cst (-5)

a = Var "a"
b = Var "b"
c = Var "c"

simpExp1 = Add one two
simpExp2 = Add negthree five
simpExp3 = Sub three five
simpExp4 = Sub one negone
simpExp5 = Mul two three
simpExp6 = Mul one negfive
simpExp7 = Mul zero five
simpExp8 = Div five two
simpExp9 = Div one zero 
simpExp10 = Pow two two
simpExp11 = Pow five zero
simpExp12 = Pow simpExp9 zero
simpExp13 = Pow five negone

advExp1 = If zero simpExp1 simpExp2
advExp2 = If one simpExp1 simpExp2
advExp3 = If simpExp9 simpExp1 simpExp2

varExp1 = Add a one
varExp2 = Mul b two

advExp4 = Let "a" two varExp1
advExp5 = Let "a" simpExp9 varExp1
advExp6 = Let "a" simpExp9 advExp1
advExp7 = Sum "b" zero two varExp2
advExp8 = Sum "b" zero two simpExp1
advExp9 = Sum "b" zero two simpExp9




main :: IO ()
main = do 
    putStrLn $ showExp simpExp1 ++ " = " ++ (show $ evalSimple simpExp1)
    putStrLn $ showExp simpExp2 ++ " = " ++ (show $ evalSimple simpExp2)
    putStrLn $ showExp simpExp3 ++ " = " ++ (show $ evalSimple simpExp3)
    putStrLn $ showExp simpExp4 ++ " = " ++ (show $ evalSimple simpExp4)
    putStrLn $ showExp simpExp5 ++ " = " ++ (show $ evalSimple simpExp5)
    putStrLn $ showExp simpExp6 ++ " = " ++ (show $ evalSimple simpExp6)
    putStrLn $ showExp simpExp7 ++ " = " ++ (show $ evalSimple simpExp7)
    putStrLn $ showExp simpExp8 ++ " = " ++ (show $ evalSimple simpExp8)

    putStrLn $ showExp simpExp10 ++ " = " ++ (show $ evalSimple simpExp10)
    putStrLn $ showExp simpExp11 ++ " = " ++ (show $ evalSimple simpExp11)
    
    print $ evalFull advExp1 initEnv
    print $ evalFull advExp2 initEnv

    print $ evalFull advExp4 initEnv
    print $ evalFull advExp6 initEnv

    print $ evalFull advExp7 initEnv
    
    
    print $ evalErr simpExp9 initEnv
    print $ evalErr simpExp12 initEnv
    print $ evalErr simpExp13 initEnv
    
    print $ evalErr varExp1 initEnv
    
    print $ evalErr advExp1 initEnv
    print $ evalErr advExp2 initEnv
    print $ evalErr advExp3 initEnv
    print $ evalErr advExp4 initEnv
    print $ evalErr advExp5 initEnv
    print $ evalErr advExp6 initEnv
    print $ evalErr advExp7 initEnv
    print $ evalErr advExp8 initEnv
    print $ evalErr advExp9 initEnv
    
--  putStrLn $ showExp simpExp9 ++ " = " ++ (show $ evalSimple simpExp9)
--  putStrLn $ showExp simpExp12 ++ " = " ++ (show $ evalSimple simpExp12)
--  putStrLn $ showExp simpExp11 ++ " = " ++ (show $ evalSimple simpExp13)

--  print $ evalFull advExp3 initEnv
--  print $ evalFull advExp5 initEnv
--  print $ evalFull advExp9 initEnv

--  print $ evalFull varExp1 initEnv
