-module(test_whitebox).
-include_lib("eunit/include/eunit.hrl").
-import(district, [create/1, get_description/1, connect/3, options/1, 
                   enter/2, take_action/3, activate/1, shutdown/2]).

%% test setup for the configuration state
d_config_test_() ->
    {setup,
     fun build_districts/0,
     fun end_districts/1,
     fun d_config_testing/1}.

build_districts() -> 
    {_, D1} = create("d1"),
    {_, D2} = create("d2"),
    {_, D3} = create("d3"),
    {_, D4} = create("d4"),
    {D1, D2, D3, D4}.

end_districts({D1, D2, D3, D4}) ->
    D1 ! {terminate},
    D2 ! {terminate},
    D3 ! {terminate},
    D4 ! {terminate}.
     
%% Tests districts in the configuration state and the transition
%% to the active state
d_config_testing({D1, D2, D3, D4}) ->
    [?_assertEqual(get_description(D1), {ok, "d1"}),
     ?_assertEqual(connect(D1, a1, D2), ok),
     ?_assertEqual(connect(D2, a1, D3), ok),
     ?_assertEqual(connect(D3, a1, D4), ok),
     ?_assertEqual(connect(D1, a1, D2), {error, is_key}),
     ?_assertEqual(options(D1), {ok, [a1]}),
     ?_assertEqual(options(D4), {ok, []}),
     ?_assertEqual(enter(D1, {make_ref(), maps:new()}), {error, in_config}),
     ?_assertEqual(take_action(D1, make_ref(), a1), {error, in_config}),
     ?_assertEqual(activate(D4), active),
     ?_assertEqual(get_description(D4), {ok, "d4"}),
     ?_assertEqual(connect(D4, a2, D1), {error, is_active}),
     ?_assertEqual(options(D4), {ok, []}),
     ?_assertEqual(enter(D4, {make_ref(), maps:new()}), ok),
     ?_assertEqual(activate(D1), active)
    ].

d_active_test_() ->
    {setup,
     fun build_districts_ext/0,
     fun end_districts_ext/1,
     fun d_active_testing/1}.
     
build_districts_ext() ->
    {D1, D2, D3, D4} = build_districts(),
    C1 = {make_ref(), maps:new()},
    connect(D1, a1, D2),
    connect(D2, a1, D3),
    connect(D3, a1, D1),
    {C1, D1, D2, D3, D4}.
    
end_districts_ext({_, D1, D2, D3, D4}) ->
    D1 ! {terminate},
    D2 ! {terminate},
    D3 ! {terminate},
    D4 ! {terminate}.

%% Tests districts in the active state and in the transition
%% to the shutdown state, as well as confirming that the shutdown
%% state appears to work.
d_active_testing({{Cref, Cs}, D1, D2, D3, D4}) ->
    [?_assertEqual(activate(D1), active),
     ?_assertEqual(enter(D2, {Cref, Cs}), ok),
     ?_assertEqual(take_action(D2, Cref, a5), {error, no_such_action}),
     ?_assertEqual(take_action(D2, make_ref(), a1), {error, no_such_creature}),
     ?_assertEqual(enter(D3, {Cref, Cs}), ok),
     ?_assertEqual(take_action(D2, Cref, a1), {error, is_present}),
     ?_assertEqual(take_action(D3, Cref, a1), {ok, D1}),
     ?_assertEqual(shutdown(D4, self()), ok),
     ?_assertEqual(shutdown(D1, self()), ok),
     ?_assertEqual(enter(D1, {Cref, Cs}), {error, timeout})
    ].