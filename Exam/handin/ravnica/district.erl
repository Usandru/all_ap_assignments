-module(district).

-export([create/1,
         get_description/1,
         connect/3,
         activate/1,
         options/1,
         enter/2,
         take_action/3,
         shutdown/2,
         trigger/2,
         %% the following are my functions that I found it necessary to export
         d_config/3,
         activator/3,
         shut_down_spawn/4
         ]).

-type passage() :: pid().
-type creature_ref() :: reference().
-type creature_stats() :: map().
-type creature() :: {creature_ref(), creature_stats()}.
-type trigger() :: fun((entering | leaving, creature(), [creature()])
                                                -> {creature(), [creature()]}).


-spec create(string()) -> {ok, passage()} | {error, any()}.
create(_Desc) ->
    D_id = spawn(district, d_config, [_Desc, maps:new(), normal]),
    {ok, D_id}.
            
            
%% Process for handling the configuration state.
d_config(Desc, PMap, State) ->
    receive
        %% Handler for "get_description"
        {desc, Sender} ->
            Sender ! {ok, Desc},
            d_config(Desc, PMap, State);
            
            
        %% Handler for "connect"
        {connect, Action, To, Sender} ->
            PMap_ = case maps:is_key(Action, PMap) of
                 true -> Sender ! {error, is_key},
                         PMap;
                 false -> Sender ! {ok},
                          maps:put(Action, To, PMap)
            end,
            d_config(Desc, PMap_, State);
            
            
        %% Handler for "options"
        {options, Sender} ->
            Sender ! {ok, maps:keys(PMap)},
            d_config(Desc, PMap, State);
            
            
        %% Handler for "enter"
        {enter, _, Sender} ->
            Sender ! {error, in_config},
                     d_config(Desc, PMap, State);
            
            
        %% Handler for "take_action"
        {act, _, _, Sender} ->
            Sender ! {error, in_config},
            d_config(Desc, PMap, State);
            
        
        %% Handler for "activate"
        {activate, Sender, Tag} ->
            case State of
                normal -> case length(maps:keys(PMap)) of
                              Len when Len == 0 -> 
                                  Sender ! {active, Tag},
                                  d_active(Desc, PMap, maps:new());
                              Len when Len > 0 -> 
                                  spawn(district, activator,
                                        [maps:values(PMap),
                                        self(), {Sender, Tag}]),
                                  d_config(Desc, PMap, inter)
                          end;
                inter -> Sender ! {under_activation, Tag},
                         d_config(Desc, PMap, State)
            end;
        {finish_activate, {Sender, Tag}} -> Sender ! {active, Tag},
                                            d_active(Desc, PMap, maps:new());
        {activate_failed, {Sender, Tag}} -> Sender ! {impossible, Tag},
                                            d_config(Desc, PMap, normal);
        
        %% Handler for "shutdown".        
        {shutdown, NextPlane, Sender, Tag} ->
            NextPlane ! {shutting_down, self(), maps:new()},
            case length(maps:keys(PMap)) of
                Len when Len == 0 -> 
                    Sender ! {ok, Tag},
                    exit(shutdown);
                Len when Len > 0 -> 
                    spawn(district, shut_down_spawn, 
                          [maps:values(PMap), self(),
                          NextPlane, {Sender, Tag}]),
                    d_shutdown(Desc)
            end
    end.

%% Initial process for the activation handler, which gathers
%% callbacks from other districts until it can confirm that all
%% neighbours are either active or activating.
%% This part sends the "activate" signal to neighbours
%% before transiting into the waiting state.
activator(D_id_list, Origin, OTag) ->
    Tag_list = lists:map(fun(X) -> ping_active(X) end, D_id_list),
    Bool_list = lists:map(fun(X) -> make_bool(X) end, D_id_list),
    Zip_list = lists:zip(Tag_list, Bool_list),
    active_receiver(maps:from_list(Zip_list), Origin, OTag).
    
%% The receiver part of the activation handler. It listens
%% for any message that fits the "activate" command and matches
%% them with the tags it has. Whenever it updates the list of
%% Tags, it checks if it is done, and if so, it pings back to its
%% mother process.
active_receiver(Tag_map, Origin, OTag) ->
    case lists:all(fun(X) -> is_true(X) end, maps:values(Tag_map)) of
        true -> Origin ! {finish_activate, OTag};
        false -> receive
                     {active, Tag} -> 
                         active_receiver(maps:update(Tag, true, Tag_map),
                                         Origin, OTag);
                     {under_activation, Tag} ->
                         active_receiver(maps:update(Tag, true, Tag_map),
                                         Origin, OTag);
                     {impossible, _} -> Origin ! {activate_failed, OTag}
                 after
                     1000 -> Origin ! {activate_failed, OTag}
                 end
    end.

%% An equivalent handler to "activator", just for shutting down instead.
%% Works essentially by the same principles, except that timeout is assumed
%% to be a success, since a shutdown process will be unable to respond.
%% This is obviously not actually safe, but districts have no knowledge of
%% connections they don't reciprocate, and so there may be dangling connections
%% in the connection graph.
shut_down_spawn(D_id_list, Origin, NextPlane, OTag) ->
    Tag_list = lists:map(fun(X) -> ping_close(NextPlane, X) end, D_id_list),
    Bool_list = lists:map(fun(X) -> make_bool(X) end, D_id_list),
    Zip_list = lists:zip(Tag_list, Bool_list),
    shutdown_receiver(maps:from_list(Zip_list), Origin, OTag).
    
shutdown_receiver(Tag_map, Origin, OTag) ->
    case lists:all(fun(X) -> is_true(X) end, maps:values(Tag_map)) of
        true -> Origin ! {finish_shutdown, OTag};
        false -> receive
                     {ok, Tag} -> 
                         active_receiver(maps:update(Tag, true, Tag_map),
                                         Origin, OTag);
                     {shutting_down, Tag} -> 
                         active_receiver(maps:update(Tag, true, Tag_map),
                                         Origin, OTag)
                 after
                     1000 -> Origin ! {finish_shutdown, OTag}
                 end
    end.

%% Helper functions
make_bool(_) ->
    false.
    
is_true(Bool) ->
    Bool.
    
ping_active(D_id) ->
    Tag = make_ref(),
    D_id ! {activate, self(), Tag},
    Tag.

ping_close(NextPlane, D_id) ->
    Tag = make_ref(),
    D_id ! {shutdown, NextPlane, self(), Tag},
    Tag.    
    
%% Process for handling the configuration state.
d_shutdown(Desc) ->
    receive
        %% Handler for "get_description"
        {desc, Sender} ->
            Sender ! {ok, Desc},
            d_shutdown(Desc);
            
            
        %% Handler for "connect"
        {connect, _, _, Sender} ->
            Sender ! {error, shutting_down},
            d_shutdown(Desc);
            
            
        %% Handler for "options"
        {options, Sender} ->
            Sender ! {none},
            d_shutdown(Desc);
            
            
        %% Handler for "enter"
        {enter, _, Sender} ->
            Sender ! {error, shutting_down},
            d_shutdown(Desc);
            
            
        %% Handler for "take_action"
        {act, _, _, Sender} ->
            Sender ! {error, shutting_down},
            d_shutdown(Desc);
            
        
        %% Handler for "activate"
        {activate, Sender, Tag} ->
            Sender ! {impossible, Tag},
            d_shutdown(Desc);
            
        %% Handler for "shut_down"
        {shutdown, _, Sender, Tag} ->
            Sender ! {shutting_down, Tag},
            d_shutdown(Desc);
        {finish_shutdown, {Sender, Tag}} -> Sender ! {ok, Tag},
                                            exit(shutdown)
    end.
    
%% Process for handling the active state
d_active(Desc, PMap, CMap) ->
    receive
        %% Handler for "get_description"
        {desc, Sender} ->
            Sender ! {ok, Desc},
            d_active(Desc, PMap, CMap);
            
            
        %% Handler for "connect"
        {connect, _, _, Sender} ->
            Sender ! {error, is_active},
            d_active(Desc, PMap, CMap);
            
            
        %% Handler for "options"
        {options, Sender} ->
            Sender ! {ok, maps:keys(PMap)},
            d_active(Desc, PMap, CMap);
            
            
        %% Handler for "enter"
        {enter, {C_ref, C_stats}, Sender} ->
            CMap_ = case maps:is_key(C_ref, CMap) of
                true -> Sender ! {error, is_present},
                        CMap;
                false -> Sender ! {ok},
                         maps:put(C_ref, C_stats, CMap)
            end,
            d_active(Desc, PMap, CMap_);
            
            
        %% Handler for "take_action"
        {act, C_ref, Action, Sender} ->
            case maps:is_key(Action, PMap) of
                true -> case maps:is_key(C_ref, CMap) of
                            true -> 
                                case enter(maps:get(Action, PMap),
                                           {C_ref, maps:get(C_ref, CMap)}) of
                                        ok -> 
                                            Sender ! {ok, maps:get(Action,
                                                      PMap)},
                                            d_active(Desc, PMap, 
                                                     maps:remove(C_ref, CMap));
                                        {error, E} -> 
                                            Sender ! {error, E},
                                            d_active(Desc, PMap, CMap)
                                    end;
                            false -> Sender ! {error, no_such_creature},
                                     d_active(Desc, PMap, CMap)
                        end;
                false -> Sender ! {error, no_such_action},
                         d_active(Desc, PMap, CMap)
            end;
        
        %% Handler for "activate"
        {activate, Sender, Tag} ->
            Sender ! {active, Tag},
            d_active(Desc, PMap, CMap);
            
        %% Handler for "shutdown".        
        {shutdown, NextPlane, Sender, Tag} ->
            NextPlane ! {shutting_down, self(), CMap},
            case length(maps:keys(PMap)) of
                Len when Len == 0 -> 
                    Sender ! {ok, Tag},
                    exit(shutdown);
                Len when Len > 0 -> 
                    spawn(district, shut_down_spawn,
                    [maps:values(PMap), self(), NextPlane, {Sender, Tag}]),
                    d_shutdown(Desc)
            end
    end.
    
    
-spec get_description(passage()) -> {ok, string()} | {error, any()}.
get_description(D_id) ->
    D_id ! {desc, self()},
    receive
        {ok, Desc} -> {ok, Desc}
    after
        1000 -> {error, timeout}
    end.

-spec connect(passage(), atom(), passage()) -> ok | {error, any()}.
connect(From, Action, To) ->
    From ! {connect, Action, To, self()},
    receive
        {ok} -> ok;
        {error, Err} -> {error, Err}
    after
        1000 -> {error, timeout}
    end.

-spec activate(passage()) -> active | under_activation | impossible.
activate(D_id) ->
    Tag = make_ref(),
    D_id ! {activate, self(), Tag},
    receive
        {active, Tag} -> active;
        {impossible, Tag} -> impossible;
        {under_activation, Tag} -> under_activation
    end.


-spec options(passage()) -> {ok, [atom()]} | none.
options(D_id) ->
    D_id ! {options, self()},
    receive
        {ok, Actions} -> {ok, Actions};
        {error} -> none
    after
        1000 -> none
    end.

-spec enter(passage(), creature()) -> ok | {error, any()}.
enter(D_id, Creature) ->
    D_id ! {enter, Creature, self()},
    receive
        {ok} -> ok;
        {error, Reason} -> {error, Reason}
    after
        1000 -> {error, timeout}
    end.

-spec take_action(passage(), creature_ref(), atom()) -> {ok, passage()} | {error, any()}.
take_action(D_id, C_ref, Action) ->
    D_id ! {act, C_ref, Action, self()},
    receive
        {ok, To} -> {ok, To};
        {error, Reason} -> {error, Reason}
    after
        1000 -> {error, timeout}
    end.

-spec shutdown(passage(), pid()) -> ok.
shutdown(D_id, NextPlane) ->
    Tag = make_ref(),
    D_id ! {shutdown, NextPlane, self(), Tag},
    receive
        {ok, Tag} -> ok
    after
        1000 -> ok
    end.

-spec trigger(passage(), trigger()) -> ok | {error, any()} | not_supported.
trigger(_, _) ->
    not_supported.
