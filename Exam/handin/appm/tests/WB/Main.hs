module Main where

-- White-box unit-testing

import Defs
import ParserImpl
import Utils
--import SolverImpl (install)

import Text.Parsec (ParseError, parse)
import Text.Parsec.String (Parser)
import Text.Parsec.Char (oneOf, char, digit, satisfy)
import Text.Parsec.Combinator

import Test.Tasty
import Test.Tasty.HUnit

-- function based on:
-- https://jakewheat.github.io/intro_to_parsing/#_automated_testing_framework
makeTest :: (Eq a, Show a) => Parser a -> (String,String,a) -> TestTree
makeTest parser (src,ttype,expected) = testCase (src ++ ttype ++ show expected)  $ do
    let parseOut = parse (whitespace *> parser <* eof) "" src
    case parseOut of
      Left e -> assertFailure $ show e
      Right got -> assertEqual src expected got

-- just a sample; feel free to replace with your own structure
tests = testGroup "Tests"
  [ parserTests
  , utilityTests
  ]

parserTests = testGroup "Parser tests"
  [ testGroup "Num tests" 
      [ makeTest num ("1234567890"," = " , 1234567890)
      , makeTest num ("000345"," = " , 345)
      , makeTest num ("abc"," should fail " , 0)
      ]
  , testGroup "PName tests"
      [ makeTest pName ("a-1", " = ", P "a-1")
      , makeTest pName ("-a-1", " should fail ", P "")
      , makeTest pName ("12ac", " should fail ", P "")
      ]
  , testGroup "string tests"
      [ makeTest stringParse ("\"abc 234 #\"", " = " , "abc 234 #")
      , makeTest stringParse ("abc 234 #\"", " should fail " , "")
      ]
  , testGroup "Version tests"
      [ testCase "VNum" $
          parse vNumParse "" "123ff" @?= Right (VN 123 "ff")
      , testCase "VNum" $
          parse vNumParse "" "123" @?= Right (VN 123 "")
      , testCase "Version" $
          parse versionParse "" "123ff.4a.3.0.001" @?= Right v1
      ]
  , testGroup "Keyword tests"
      [ makeTest nameKW ("name   a-1  ", " = ", Name (P "a-1"))
      , makeTest descriptionKW (p1, " = ", (Desc "a#4 t5,."))
      , makeTest versionKW ("version  123ff.4a.3.0.001  ", " = ", Ver v1)
      , makeTest requiresKW ("requires p1 < 1.1", " = ", c1)
      , makeTest conflictsKW ("conflicts p1 < 1.1", " = ", c2)
      ]
  , testGroup "Constraint tests"
      [ makeTest greaterThan (">= 123ff.4a", " = ", vPair1)
      , makeTest lessThan ("< 123ff.4a", " = ", vPair2)
      , makeTest requiresParse ("p1 >= 123ff.4a", " = ", c3)
      , makeTest conflictsParse ("p2 < 123ff.4a", " = ", c4)
      , makeTest requireList ("p1 < 10, p2 >= 2, p1 >= 5", " = ", Just c5)
      , makeTest conflictList ("p1 < 10, p2 >= 2, p1 >= 5", " = ", Just c6)
      , makeTest requireList ("p1, p2, p3", " = ", Just c7)
      ]
  , makeTest clauses ("name p1 ;name p2; name p3;", " = ", clause_out)
  ]
  where
    v1 = V [VN 123 "ff", VN 4 "a", VN 3 "", VN 0 "", VN 1 ""]
    vPair1 = (maxV, V [VN 123 "ff", VN 4 "a"])
    vPair2 = (V [VN 123 "ff", VN 4 "a"], minV)
    p1 = "description  \"a#4 t5,.\"  "
    c1 = Constraint [(P "p1", (True, V [VN 1 "", VN 1 ""], minV))]
    c2 = Constraint [(P "p1", (False, V [VN 1 "", VN 1 ""], minV))]
    c3 = [(P "p1", (True, maxV, V [VN 123 "ff", VN 4 "a"]))]
    c4 = [(P "p2", (False, V [VN 123 "ff", VN 4 "a"], minV))]
    c5 = [(P "p1", (True, V [VN 10 ""], V [VN 5 ""])), 
          (P "p2", (True, maxV, V [VN 2 ""]))]
    c6 = [(P "p1", (False, V [VN 10 ""], V [VN 5 ""])),
          (P "p2", (False, maxV, V [VN 2 ""]))]
    c7 = [(P "p1", (True, maxV, minV)), (P "p2", (True, maxV, minV)),
          (P "p3", (True, maxV, minV))]
    clause_out = [Name $ P "p1", Name $ P "p2", Name $ P "p3"]
   
utilityTests = testGroup "Utility tests"
  [ testGroup "Version ordering tests"
      [ testCase "equal" $
          v3 == v3 @?= True
      , testCase "less" $
          v3 < v4 @?= True
      , testCase "greater" $
          v5 > v6 @?= True
      ]
  , testGroup "PConstr merge tests"
      [ testCase "MergeNoChange" $
          merge_pconstr (True, v1, v2) (False, v3, v2) @?= (True, v1, v2)
      , testCase "MergeChange" $
          merge_pconstr (False, v7, v1) (False, v5, v2) @?= (False, v5, v1)
      ,  testCase "MergeTooSmall" $
          merge_pconstr (False, v7, v5) (False, v1, v2) @?= (False, v1, v5)
      ]
  , testGroup "Full merge tests"
      [ testCase "MergeNoChange" $
          merge cList1 cList2 @?= Just cList1
      , testCase "MergeChange" $
          merge cList1 cList3 @?= Just [(P "p1", c1), (P "p2", c5), 
                                        (P "p3", c3), (P "p4", c3)]
      , testCase "MergeFail" $
          merge cList1 cList4 @?= Nothing
      ]
  ]
  where
    v1 = V [VN 5 ""]
    v2 = V [VN 3 "b"]
    v3 = V [VN 10 "abc"]
    v4 = V [VN 10 "abc", VN 3 ""]
    v5 = V [VN 10 "dbc"]
    v6 = V [VN 10 "abc", VN 3 "f"]
    v7 = V [VN 15 "qq", VN 4 ""]
    c1 = (True, v1, v2)
    c2 = (False, v7, v2)
    c3 = (True, maxV, minV)
    c4 = (True, v6, minV)
    c5 = (True, v6, v2)
    c6 = (True, minV, maxV)
    cList1 = [(P "p1", c1), (P "p2", c2), (P "p3", c3)]
    cList2 = [(P "p2", c2), (P "p1", c1), (P "p3", c3)]
    cList3 = [(P "p2", c4), (P "p4", c3)]
    cList4 = [(P "p2", c6), (P "p3", c1)]
   
main = defaultMain tests

