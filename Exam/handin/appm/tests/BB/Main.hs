module Main where

-- Put your black-box tests in this file

import Defs
import Parser (parseDatabase)
import Solver (install)

import Test.Tasty
import Test.Tasty.HUnit

-- just a sample; feel free to replace with your own structure
tests = testGroup "Unit tests"
  [testGroup "Parser tests"
     [testCase "tiny" $
        parseDatabase "package {name foo}" @?= Right db,
      testCase "full" $
        parseDatabase parseText @?= Right db2],
   testGroup "Solver tests"
     [testCase "tiny" $
        install db pname @?= Just [(pname, ver)] ] ]
  where
    pname = P "foo"
    ver = V [VN 1 ""]
    db = DB [Pkg pname ver "" []]
    parseText = "package {name p1; version 1f.2d;\n\
                 \description \"te st -- 32\" ;\n\
                 \requires p2 < 4ff.6 , p2 >= 1 , p3;conflicts p4<4;}\
                 \ package{version 3;name p2} \n\
                 \package {name p3}package {name p4;}"
    p1 = P "p1"
    p2 = P "p2"
    p3 = P "p3"
    p4 = P "p4"
    v1 = V [VN 3 ""]
    v2 = V [VN 1 "f", VN 2 "d"]
    v3 = V [VN 4 "ff", VN 6 ""]
    v4 = V [VN 1 ""]
    v5 = V [VN 4 ""]
    db2 = DB [Pkg p1 v2 "te st -- 32" [(p2, (True, v3, v4)), 
                                       (p3, (True, maxV, minV)),
                                       (p4, (False, v5, minV))],
                                       Pkg p2 v1 "" [],
                                       Pkg p3 ver "" [],
                                       Pkg p4 ver "" []]
                 
main = defaultMain tests

