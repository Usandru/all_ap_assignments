module ParserImpl where

-- put your parser in this file. Do not change the types of the following
-- exported functions

import Defs
import Utils
import Text.Parsec
import Text.Parsec.String
import Text.Parsec.Char
import Text.Parsec.Combinator
import Data.Char
import Data.Maybe
import Control.Monad
import Control.Applicative ((<*),(<$>), (*>), (<|>),(<$),(<*>))

parseVersion :: String -> Either ErrMsg Version
parseVersion s = case parse versionParse "" s of
                      Left _ -> Left "The parser encountered an error"
                      Right v -> Right v

parseDatabase :: String -> Either ErrMsg Database
parseDatabase s = case parse database "" s of
                       Left _ -> Left "The parser encountered an error"
                       Right db -> Right db

-- Wrapper datatype to allow for easier parsing of unordered clauses
data Clause = Name PName | Desc String | Constraint Constrs | Ver Version
  deriving (Eq, Show, Read)

-- a significant amount of this code adapts code and code fragments 
-- found here: https://jakewheat.github.io/intro_to_parsing
-- In particular, the following three parsers were taken directly from
-- that site.
whitespace :: Parser ()
whitespace = void $ many $ oneOf " \n\t"

lexeme :: Parser a -> Parser a
lexeme p = p <* whitespace

symbol :: Char -> Parser Char
symbol c = lexeme $ char c

-- Terminal parsers:
num :: Parser Int
num = do
    n <- many1 digit
    return (read n)
    
pName :: Parser PName
pName = do
    fc <- firstChar
    rest <- many nonFirstChar
    return $ P (fc:rest)
  where
    firstChar = satisfy (\a -> isLetter a)
    nonFirstChar = satisfy (\a -> isDigit a || isLetter a || a == '-')
    
stringParse :: Parser String
stringParse = do
    quote
    rest <- many anyChar -- handling "" needs a try
    quote
    return rest
  where
    quote = satisfy (\a -> a == '\"')
    anyChar = satisfy (\a -> a /= '\"')
    
-- Composites for terminals into low-level AST values
vNumParse :: Parser VNum
vNumParse = do
    number <- num
    string <- many chars
    return (VN number string)
  where
    chars = satisfy (\a -> isLetter a)
    
versionParse :: Parser Version
versionParse = do
    version <- vNumParse `sepBy` char '.'
    return (V version)
    
greaterThan :: Parser (Version, Version)
greaterThan = do
    void $ char '>'
    void $ char '='
    whitespace
    ver <- lexeme versionParse
    return (maxV, ver)

lessThan :: Parser (Version, Version)
lessThan = do
    void $ char '<'
    whitespace
    ver <- lexeme versionParse
    return (ver, minV)
    
-- special case for Constraint parsers where there
-- is nothing to parse.
emptyConst :: Parser (Version, Version)
emptyConst = do
    return (maxV, minV)

-- Builders for single dependency constraint lists
requiresParse :: Parser Constrs
requiresParse = do
    name <- lexeme pName
    (max, min) <- lexeme $ choice [greaterThan, lessThan, emptyConst]
    return [(name, (True, max, min))]

conflictsParse :: Parser Constrs
conflictsParse = do
    name <- lexeme pName
    (max, min) <- lexeme $ choice [greaterThan, lessThan, emptyConst]
    return [(name, (False, max, min))]

-- builders for multiple dependency constraint lists
-- using merge to combine values from single dependency parsers
requireList :: Parser (Maybe Constrs)
requireList = do
    (x:xs) <- requiresParse `sepBy` symbol ','
    return $ foldM merge x xs

conflictList :: Parser (Maybe Constrs)
conflictList = do
    (x:xs) <- conflictsParse `sepBy` symbol ','
    return $ foldM merge x xs

-- Collects all clauses in a package
clauses :: Parser [Clause]
clauses = do
    clauseList <- (choice [nameKW, versionKW, descriptionKW, 
                           requiresKW, conflictsKW]) `sepEndBy` symbol ';'
    return clauseList

-- Builds a full database by collecting a list of package ASTs.
-- The top-level parser.
database :: Parser Database
database = do
    whitespace
    db <- many $ lexeme package
    return $ DB db
    
-- Wrapper for buildPkg, initializing the tuples correctly
-- with bools and default values.
buildPkgWrap :: [Clause] -> Maybe Pkg
buildPkgWrap clauses = buildPkg clauses (False, "") (False, P "")
                                (False, V [VN 1 ""]) []
    
-- Function that builds a Pkg from a list of clauses. It requires 
-- that the clauses contain exactly one PName, and may contain at
-- most 1 desc and version. This is controlled with the bools in the
-- tuples. The dependencies are simply merged and thus require no
-- special handlings, except for one edge-case which is not dealt with:
-- dependencies that are self-referential are not caught as they must
-- be checked after the Pkg is completely built.
buildPkg :: [Clause] -> (Bool, String) -> (Bool, PName) -> 
            (Bool, Version) -> Constrs -> Maybe Pkg
buildPkg [] (_, s) (b, n) (_, v) c = if b then Just (Pkg {name = n, desc = s,
                                                     ver = v, deps = c})
                                          else Nothing
buildPkg ((Desc s):xs) (b, _) pn pv c = if b then Nothing
                                               else buildPkg xs (True, s)
                                                             pn pv c
buildPkg ((Name n):xs) pd (b, _) pv c = if b then Nothing
                                             else buildPkg xs pd (True, n)
                                                           pv c
buildPkg ((Ver v):xs) pd pn (b, _) c = if b then Nothing
                                            else buildPkg xs pd pn (True, v)
                                                          c
buildPkg ((Constraint c1):xs) pd pn pv c2 = case merge c1 c2 of
                                                 Nothing -> Nothing
                                                 Just c -> buildPkg xs pd pn
                                                                    pv c

-- Clause parsers for keyword + clause items. Since there were only
-- six of them, and handling differences in letter case was non-obvious
-- the keywords were simply hardcoded.
nameKW :: Parser Clause
nameKW = do
    void $ oneOf "nN"
    void $ oneOf "aA"
    void $ oneOf "mM"
    void $ oneOf "eE"
    void $ char ' '
    whitespace
    name <- lexeme pName
    return (Name name)
    
versionKW :: Parser Clause
versionKW = do
    void $ oneOf "vV"
    void $ oneOf "eE"
    void $ oneOf "rR"
    void $ oneOf "sS"
    void $ oneOf "iI"
    void $ oneOf "oO"
    void $ oneOf "nN"
    void $ char ' '
    whitespace
    ver <- lexeme versionParse
    return (Ver ver)
    
descriptionKW :: Parser Clause
descriptionKW = do
    void $ oneOf "dD"
    void $ oneOf "eE"
    void $ oneOf "sS"
    void $ oneOf "cC"
    void $ oneOf "rR"
    void $ oneOf "iI"
    void $ oneOf "pP"
    void $ oneOf "tT"
    void $ oneOf "iI"
    void $ oneOf "oO"
    void $ oneOf "nN"
    void $ char ' '
    whitespace
    s <- lexeme stringParse
    return (Desc s)
    
requiresKW :: Parser Clause
requiresKW = do
    void $ oneOf "rR"
    void $ oneOf "eE"
    void $ oneOf "qQ"
    void $ oneOf "uU"
    void $ oneOf "iI"
    void $ oneOf "rR"
    void $ oneOf "eE"
    void $ oneOf "sS"
    whitespace
    cList <- requireList
    return (Constraint (fromJust cList))
    
conflictsKW :: Parser Clause
conflictsKW = do
    void $ oneOf "cC"
    void $ oneOf "oO"
    void $ oneOf "nN"
    void $ oneOf "fF"
    void $ oneOf "lL"
    void $ oneOf "iI"
    void $ oneOf "cC"
    void $ oneOf "tT"
    void $ oneOf "sS"
    whitespace
    cList <- conflictList
    return (Constraint (fromJust cList))
    
package :: Parser Pkg
package = do
    void $ oneOf "pP"
    void $ oneOf "aA"
    void $ oneOf "cC"
    void $ oneOf "kK"
    void $ oneOf "aA"
    void $ oneOf "gG"
    void $ oneOf "eE"
    whitespace
    cList <- between (symbol '{') (symbol '}') clauses
    return $ fromJust $ buildPkgWrap cList