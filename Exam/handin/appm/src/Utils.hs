module Utils where

-- Any auxiliary code to be shared by Parser, Solver, or tests
-- should be placed here.

import Defs
import qualified Data.Map.Strict as Map

instance Ord Version where
  compare (V []) (V []) = EQ
  compare (V []) (V _) = LT
  compare (V _) (V []) = GT
  compare (V ((VN xI xS):xs)) (V ((VN yI yS):ys))
     | xI < yI = LT
     | xI > yI = GT
     | (length xS) < (length yS) = LT
     | (length xS) > (length yS) = GT
     | xS < yS = LT
     | xS > yS = GT
     | otherwise = compare (V xs) (V ys)

-- The wrapper for all the constraint merge activity
merge :: Constrs -> Constrs -> Maybe Constrs
merge a b = if merge_check result then Just result
                                  else Nothing
  where
      result = merge_aux a b

-- The actual merger function
merge_aux :: Constrs -> Constrs -> Constrs
merge_aux a b = Map.toList $ Map.unionWith merge_pconstr 
                                           (Map.fromList a) (Map.fromList b)

-- Constraints must satisfy that there are no constraints that simultaneously
-- have their min values greater than their max value and are noted as required
-- packages (ie. the form (True, (1, 2)) is illegal) since such a constraint
-- cannot ever be satisfied.
merge_check :: Constrs -> Bool
merge_check [] = True
merge_check ((_, (b, max, min)):xs) = if (max < min) && b then False
                                                          else merge_check xs

-- given two constraints, return true if either is true, false
-- otherwise, and the intersection of the two intervals in any case
merge_pconstr :: PConstr -> PConstr -> PConstr
merge_pconstr (xBool, xMax, xMin) (yBool, yMax, yMin)
     | xBool || yBool = (True, minVer xMax yMax, maxVer xMin yMin)
     | otherwise = (False, minVer xMax yMax, maxVer xMin yMin)

-- a pair of functions that serve as shorthand in merge_pconstr
maxVer :: Version -> Version -> Version
maxVer x y
     | x < y = y
     | x >= y = x
     
minVer :: Version -> Version -> Version
minVer x y
     | x < y = x
     | x >= y = y